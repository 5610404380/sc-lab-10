package G7;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;

   private static final double DEFAULT_RATE = 5;
   private static final double INITIAL_BALANCE = 1000;   

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JButton button1;
   private JTextArea resultLabel;
   private JPanel panel;
   private BankAccount account;
   
   public static void main(String[] args){
	   new InvestmentFrame();
   }
   
   public InvestmentFrame()
   {  
      account = new BankAccount(INITIAL_BALANCE);

      // Use instance variables for components 
      resultLabel = new JTextArea(5,10);
      resultLabel.setText("Balance: " + account.getBalance()+"\n");
      // Use helper methods 
      createTextField();
      createButton();
      createButton1();
      createPanel();
      
      setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }

   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   private void createButton()
   {
      button = new JButton("Deposit");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            double rate = Double.parseDouble(rateField.getText());
            account.deposit(rate);
            resultLabel.append("Deposit: " + rate+"\n");
            resultLabel.append("Balance: " + account.getBalance()+"\n");
         }            
      }
      
      ActionListener listener = new AddInterestListener();
      button.addActionListener(listener);
   }
   private void createButton1()
   {
      button1 = new JButton("Withdraw");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            double rate = Double.parseDouble(rateField.getText());
            account.withdraw(rate);
            resultLabel.append("Withdraw: " + rate +"\n");
            resultLabel.append("Balance: " + account.getBalance()+"\n");
         }            
      }
      
      ActionListener listener = new AddInterestListener();
      button1.addActionListener(listener);
   }

   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(button1);
      panel.add(resultLabel);      
      add(panel);
   } 
}
