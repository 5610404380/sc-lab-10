package G2;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUIradio
{    
   private ButtonGroup bg;
   private JRadioButton b1,b2,b3;
   private JPanel panel;
   private JPanel panel1;
   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUIradio();
   }
   
   public GUIradio()
   {
	  createButton();
	  createButton1();
	  createButton2();
	  createPanel(); 
	  createPanel1(); 
	  frame.setLayout(new BorderLayout());
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(750, 500);
      frame.add(panel1,BorderLayout.CENTER);
      frame.add(panel,BorderLayout.SOUTH);
      bg = new ButtonGroup();
      bg.add(b1);
      bg.add(b2);
      bg.add(b3);
      panel.add(b1);
      panel.add(b2);
      panel.add(b3);
   }
   private void createButton(){
	  b1 = new JRadioButton("RED");
      
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 panel.setBackground(Color.RED);
        	 panel1.setBackground(Color.RED);
         }            
      }
      
      ActionListener red = new AddInterestListener();
      b1.addActionListener(red);

   }
   private void createButton1()
   {
	   b2 = new JRadioButton("BLUE");
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 panel.setBackground(Color.BLUE);
        	 panel1.setBackground(Color.BLUE);
         }            
      }
      
      ActionListener blue = new AddInterestListener();
      b2.addActionListener(blue);
   }
   private void createButton2()
   {
	   b3 = new JRadioButton("GREEN");
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 panel.setBackground(Color.GREEN);
        	 panel1.setBackground(Color.GREEN);
         }            
      }
      ActionListener green = new AddInterestListener();
      b3.addActionListener(green);
   }

   private void createPanel()
   {
	   panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 

   } 
   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}
