package G4;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUIcombobox
{    

   private JComboBox button;
   private JPanel panel;
   private JPanel panel1;
   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUIcombobox();
   }
   
   public GUIcombobox()
   {
	  createButton();
	  createPanel(); 
	  createPanel1(); 
	  frame.setLayout(new BorderLayout());
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(750, 500);
      frame.add(panel1,BorderLayout.CENTER);
      frame.add(panel,BorderLayout.SOUTH);
      panel.add(button);

   }
   private void createButton()
   {
	  String color [] = {"RED","BLUE","GREEN"};
      button = new JComboBox(color);
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 if(button.getSelectedItem() == "RED"){
        		 panel.setBackground(Color.RED);
            	 panel1.setBackground(Color.RED); 
        	 }
        	 if(button.getSelectedItem() == "BLUE"){
        		 panel.setBackground(Color.BLUE);
            	 panel1.setBackground(Color.BLUE); 
        	 }
        	 if(button.getSelectedItem() == "GREEN"){
        		 panel.setBackground(Color.GREEN);
            	 panel1.setBackground(Color.GREEN); 
        	 }

         }            
      }
      
      ActionListener colors = new AddInterestListener();
      button.addActionListener(colors);

   }

   private void createPanel()
   {
	   panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 

   } 
   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}
