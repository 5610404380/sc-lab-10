package G5;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUImenu
{    

   private JMenuBar menubar;
   private JMenu menucolor;
   private JMenuItem menub1,menub2,menub3;
   private JPanel panel1;
   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUImenu();
   }
   
   public GUImenu()
   {
	  createmenu();
	  createPanel1(); 
	  frame.setJMenuBar(menubar);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(750, 500);
      frame.add(panel1,BorderLayout.CENTER);
      menubar.add(menucolor);
      menucolor.add(menub1);
      menucolor.add(menub2);
      menucolor.add(menub3);
      

   }
   private void createmenu()
   {
     menubar = new JMenuBar();
     menucolor = new JMenu("Color");
     menub1 = new JMenuItem("RED");
     menub2 = new JMenuItem("BLUE");
     menub3 = new JMenuItem("GREEN");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 if(event.getSource() == menub1){
            	 panel1.setBackground(Color.RED); 
        	 }
        	 if(event.getSource() == menub2){
            	 panel1.setBackground(Color.BLUE); 
        	 }
        	 if(event.getSource() == menub3){
            	 panel1.setBackground(Color.GREEN); 
        	 }

         }            
      }
      
      ActionListener colors = new AddInterestListener();
      menub1.addActionListener(colors);
      menub2.addActionListener(colors);
      menub3.addActionListener(colors);

   }

   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}
